import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.*;


/**
 * Created by Ivan on 09.11.2014.
 */
public class startup {
    public static void main(String[] args) {
        switch (args.length) {
            case 0:
                printHelp();
                break;
            case 1:
                try {
                    parseFile(args[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                printHelp();
                break;
        }
    }

    public static void printHelp() {
        System.out.println("Usage: ");
    }

    public static void parseFile(String path) throws IOException {
        Reader in = new FileReader(path);

        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
        for (CSVRecord record : records) {
            String id    = record.get(0);
            String xml   = record.get(1);
            System.out.println("CSV entry: " + id);

            NodeList nodeList = parseXML(xml).getDocumentElement().getChildNodes();
            for(int i = 0; i < nodeList.getLength(); i++) {
                Node currentNode = nodeList.item(i);
                if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                    System.out.println("    " + currentNode.getNodeName() + ": " + currentNode.getTextContent());
                }
            }
            createXLS(nodeList);
        }
    }

    public static Document parseXML(String input) {
        DOMParser ps = new DOMParser();
        StringReader sr = new StringReader(input);
        try {
            ps.parse(new InputSource(sr));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ps.getDocument();
    }

    public static void createXLS(NodeList nodeList) {
        try{
            String filename="NewExcelFile.xls";
            HSSFWorkbook workbook=new HSSFWorkbook();
            HSSFSheet sheet =  workbook.createSheet("FirstSheet");

            int k = 0;
            for(int i = 0; i < nodeList.getLength(); i++) {
                Node currentNode = nodeList.item(i);
                if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                    HSSFRow rowhead = sheet.createRow((short) k);
                    rowhead.createCell(0).setCellValue(currentNode.getNodeName());
                    rowhead.createCell(1).setCellValue(currentNode.getTextContent());
                    k++;
                }
            }

            FileOutputStream fileOut =  new FileOutputStream(filename);
            workbook.write(fileOut);
            fileOut.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
